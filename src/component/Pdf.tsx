import React,  { useEffect, useState }  from 'react'
import { Link } from "react-router-dom"
import Pdf from "react-to-pdf";
import SignaturePad from "signature_pad";

import Header from "./Header"
let sigPad: any = null

export default function PdfC() {
    const ref: any = React.createRef();
    const [sigPadData, setSigPadData] = useState('');

    useEffect(() => {
   const canvas = document.querySelector("canvas")
    if(canvas){
        sigPad = new SignaturePad(canvas, {
            onBegin: () => {
            //   setSigPadData(sigPad.toDataURL('image/jpeg')); // data:image/png;base64,iVBORw0K...
              /**
               * signaturePad.toDataURL(); // save image as PNG
               * signaturePad.toDataURL("image/jpeg"); // save image as JPEG
               * signaturePad.toDataURL("image/svg+xml"); // save image as SVG
               * */
            },
            onEnd: () => {
                console.log('?')
                setSigPadData(sigPad.toDataURL()); // data:image/png;base64,iVBORw0K...
                // const file = new File([sigPadData], "foo.pdf", {
                //     type: "text/plain",
                //   });
                // html태그를 저장하고, 고객에게는 pdf 공유하도록
                // new File, ie에서는 안됩니다
                /**
                 * signaturePad.toDataURL(); // save image as PNG
                 * signaturePad.toDataURL("image/jpeg"); // save image as JPEG
                 * signaturePad.toDataURL("image/svg+xml"); // save image as SVG
                 * */
              }
            
          });
    }
    }, []);
  
    const handleRestSignature = () => {
      sigPad.clear();
      setSigPadData('');
    };
    const pdfClar = (e:any) => {
        console.log(ref.current.innerHTML)
        console.log('complete')
    }

    return (
        <>  
            <Header/>
            <h1>Home page</h1>
            <Pdf targetRef={ref} filename="code-example.pdf" onComplete = {pdfClar}>
                {({ toPdf }: any) => <button onClick={toPdf} >Generate Pdf</button>}
            </Pdf>
            {/* <Pdf>
    {({toPdf, targetRef}: any) =>  (
        <div style={{width: 500, height: 500, background: 'red'}} onClick={toPdf} ref={targetRef}/>
    )}
</Pdf> */}
        <div ref={ref}>
            월주차


            <div>서명: </div> {sigPadData?  <img src={sigPadData }/> : null}
        </div>

        <div className="Signature">
      <canvas
        width={300}
        height={325}
        style={{ border: "1px solid #cdcdcd" }}
      />
      <button onClick={handleRestSignature}>리셋</button>
    </div>
        </>
    );
}