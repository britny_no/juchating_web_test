import react from 'react'
import { Link } from "react-router-dom"

import Header from "./Header"

export default function Home() {

    return (
        <>  
            <Header/>
            <h1>Home page</h1>
            <div>
                Home!
            </div>
        </>
    );
}