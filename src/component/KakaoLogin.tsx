import react, {useState, useEffect} from 'react'
import { Link } from "react-router-dom"
import KakaoLogin from 'react-kakao-login';
import axios from 'axios'
import { GoogleLogin } from 'react-google-login';

import Header from "./Header"

const makeFormData = (params:any) => {
    const searchParams = new URLSearchParams()
        Object.keys(params).forEach(key => {
            searchParams.append(key, params[key])
        })
    return searchParams
}


export default function KLogin() {
    const [access_token, setAccessToken] = useState('')
    const [refresh_token, setRefreshToken] = useState('')

    const {Kakao}: any = window;
  

    const login = () => {
        if(process.env.REACT_APP_KAKAO_KEY && process.env.REACT_APP_KAKAO_REDIRECT_URL){
            const api_url = `https://kauth.kakao.com/oauth/authorize?client_id=${process.env.REACT_APP_KAKAO_KEY}&redirect_uri=${process.env.REACT_APP_KAKAO_REDIRECT_URL}&response_type=code`
            const child:any = window.open(api_url)

            // 자식 window close 이후 process
            child.onunload = function(){ 
                
            };
        }
    }

    const profile = () => {
        if(process.env.REACT_APP_KAKAO_KEY && process.env.REACT_APP_KAKAO_REDIRECT_URL && Kakao.Auth.getAccessToken()){
            Kakao.API.request({
                url: '/v2/user/me',
            
                success: function(response: string) {
                    console.log(response)
                },
                fail: function(error: string) {
                    const res = JSON.parse(error)
                    
                }
            });
        }
    }
    
    const refreshToken = () => {
        if(process.env.REACT_APP_KAKAO_KEY && process.env.REACT_APP_KAKAO_SECRET){
            axios({   
                method: 'POST',
                headers: {
                  'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
                },
                url: 'https://kauth.kakao.com/oauth/token',
                data: makeFormData({
                  grant_type: 'refresh_token',
                  client_id: process.env.REACT_APP_KAKAO_KEY,
                  refresh_token: localStorage.getItem('ref_kakao'),
                  client_secret: process.env.REACT_APP_KAKAO_SECRET,
                })
            })
            .then(res => {
                const data = res.data
                setAccessToken(data.access_token)
                Kakao.Auth.setAccessToken(data.access_token);
                setRefreshToken(data.refresh_token)
            })
            .catch(err => {
                console.log(err)
            })
        }
    }

    return (
        <>  
            <Header/>
            <h1>kakao-Login page</h1>
                <br/>
                <br/>
                <button onClick={login}>카카오 로그인</button>
                <br/>
                <button onClick={profile}>프로필 조회(로그인 확인용)</button>
                <br/>
                <br/>
                <button onClick={refreshToken}>refreshToken</button>
            <br/>
        </>
    );
}