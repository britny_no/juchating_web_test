import react from 'react'
import { Link } from "react-router-dom"

export default function Header() {

    return (
        <>
            <Link to="/">Home</Link>
            <br/>
            <Link to="/klogin">Kakao Login</Link>
            <br/>
            <Link to="/glogin">GoogleLogin</Link>
            <br/>
            <Link to="/nlogin">NaverLogin</Link>
            <br/>
            <Link to="/postcode">PostCode</Link>
            <br/>
            <Link to="/import">Import</Link>
            <br/>
            <Link to="/phone_verify">PhoneVerify</Link>
            <br/>
            <Link to="/security">Security</Link>
            <br/>
            <Link to="/api_test">ApiTest</Link>
            <br/>
            <Link to="/pdf">PdfTest</Link>
            <br/>


        </>
    );
}