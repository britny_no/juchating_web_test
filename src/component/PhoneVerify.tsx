import react, {useState, useEffect} from 'react'
import axios from 'axios'
// import {}
import Header from "./Header"
import Countdown from 'react-countdown';

export default function PhoneVerify({history}:any) {
    const [showCount, setShowCount] = useState(false)
    const [phoneNumber, setPhoneNumber] = useState('')
    const [verifyNumber, setVerifyNumber] = useState('')

    const sendSms= () => {
        axios.post(`${process.env.REACT_APP_API_HOST}/send_verify_number`, {
            data: phoneNumber
        })
        .then((res: any) => { 
            console.log(res)
        })
        setShowCount(true)
    }

    const requestVerify = (e: any) => {
        axios.post(`${process.env.REACT_APP_API_HOST}/verify_number`, {
            phone: phoneNumber,
            verify_number: verifyNumber
        })
        .then((res: any) => { 
            console.log(res)
        })
    }

    const changePhoneNumber = (e: any) => {setPhoneNumber(e.target.value)}
    const changeVerifyNumber = (e: any) => {setVerifyNumber(e.target.value)}

    return (
        <>  
            <Header/>
            <h1>Phone verify page</h1>
            <div>
                <input type="number" onChange={changePhoneNumber} value={phoneNumber}/><button onClick={sendSms}>인증 번호 전송</button>
                <br/> <br/> <br/>
                {showCount ? <Countdown 
                                date={Date.now() + 180000} 
                                renderer={props => <div>{props.minutes} : {String(props.seconds).length === 1 ? '0'+props.seconds  : props.seconds }</div>}
                                />: null}
                <input type="number" onChange={changeVerifyNumber} value={verifyNumber}/><button onClick={requestVerify}>인증 번호 확인</button>
                
            </div>
        </>
    );
}