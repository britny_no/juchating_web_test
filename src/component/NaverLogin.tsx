import react, {useState, useEffect} from 'react'
import axios from 'axios'
// import {}
import Header from "./Header"

export default function NLogin({history}:any) {
    const login = () => {
        if(process.env.REACT_APP_NAVER_KEY && process.env.REACT_APP_NAVER_CALLBACK_URL){
            const client_id = process.env.REACT_APP_NAVER_KEY;
            const state = "RAMDOM_STATE";
            const redirectURI = encodeURI(process.env.REACT_APP_NAVER_CALLBACK_URL);
            const api_url = 'https://nid.naver.com/oauth2.0/authorize?response_type=code&client_id=' + client_id + '&redirect_uri=' + redirectURI + '&state=' + state;
            
            const child:any = window.open(api_url)
            // 자식 window close 이후 process
            child.onunload = function(){ 
    
            };
        }
    }

    const member = () => {
        let data = {
            token: localStorage.getItem('acc')
        }
        axios.post(`${process.env.REACT_APP_API_HOST}/naver_member`, JSON.stringify(data),{
            headers: {
                'Content-Type': 'application/json'
              }
        })
        .then(res => console.log(res))
        .catch(res => console.log(res))
    }

    const refresh = () => {
        const ref = localStorage.getItem('ref')
        axios.get(`${process.env.REACT_APP_API_HOST}/naver_refresh_access?refresh_token=${ref}`)
        .then(res => {
            localStorage.setItem('acc', res.data.access_token)
            localStorage.setItem('ref', res.data.refresh_token)
        })
        .catch(res => console.log(res))
    }

    return (
        <>  
            <Header/>
            <h1>Naver-Login page</h1>
            <div>
            <div>Naver-Login login result:</div>
            <br/>
            <button onClick={login}>네이버 로그인</button>
            <br/>
            <br/>
            <button onClick={member}>프로필 조회(로그인 확인용)</button>
            <br/>
            <br/>
            <button onClick={refresh}>리프레시 재발급</button>
            </div>
        </>
    );
}