import react, {useState, useEffect} from 'react'
import axios from 'axios'
import { useGoogleLogin, GoogleLogin } from 'react-google-login';

import Header from "./Header"

export default function GLogin() {
    const [login_state, setLoginState] = useState('')
    
    const responseGoogle = async (res: any) => {
        setLoginState(JSON.stringify(res))
        // await res.reloadAuthResponse();
    }

    return (
        <>  
            <Header/>
            <h1>Google-Login page</h1>
            <div>
            <div>Google-Login login result:</div>
            {login_state}
            <br/>
            {process.env.REACT_APP_GOOGLE_KEY ? 
                        <GoogleLogin
                        clientId={process.env.REACT_APP_GOOGLE_KEY}
                        buttonText={"Login"}
                        prompt={"consent"}
                        scope={"profile"}
                        responseType={"id_token"}
                        onSuccess={responseGoogle}
                        onFailure={responseGoogle}
                        cookiePolicy={'single_host_origin'}
                    />
                    : null}
            </div>
        </>
    );
}