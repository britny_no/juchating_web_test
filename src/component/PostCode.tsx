import react, {useEffect} from 'react'
import { Link } from "react-router-dom"
import DaumPostcode from 'react-daum-postcode';
import Header from "./Header"

export default function PostCode() {
  const {kakao}: any = window;
  useEffect(() => {
    // const map = new Kakao.maps.Map(mapContainer, mapOption); 
    console.log(navigator.geolocation)

  }, [])
    const handleComplete = (data: any) => {
        let fullAddress = data.address;
        let extraAddress = ''; 
        
        if (data.addressType === 'R') {
          if (data.bname !== '') {
            extraAddress += data.bname;
          }
          if (data.buildingName !== '') {
            extraAddress += (extraAddress !== '' ? `, ${data.buildingName}` : data.buildingName);
          }
          fullAddress += (extraAddress !== '' ? ` (${extraAddress})` : '');
        }
    
        console.log(fullAddress);  // e.g. '서울 성동구 왕십리로2길 20 (성수동1가)'


        // kakao map sdk 사용
        const geocoder = new kakao.maps.services.Geocoder();

        // 주소로 좌표를 검색합니다
        geocoder.addressSearch(fullAddress, function(result: any, status: any) {
    
            // 정상적으로 검색이 완료됐으면 
            if (status === kakao.maps.services.Status.OK) {
                console.log(result)
            } 
        });  
      }
    

    return (
        <>  
            <Header/>
            <h1>PostCode page</h1>
            <div>
                PostCode!
                <DaumPostcode
                    onComplete={handleComplete}

                    
                    />
                <br/>
            </div>
        </>
    );
}