import react, {useState, useEffect} from 'react'
import { Link } from "react-router-dom"
import KakaoLogin from 'react-kakao-login';
import axios from 'axios'
import { GoogleLogin } from 'react-google-login';
import jwt from 'jsonwebtoken';
import Header from "./Header"

import {randomString} from '../js/index'

export default function Api() {
    const [files, setFiles] = useState('')
    const [img, setImg] = useState<Array<string>>([])
    useEffect(() => {
        // https or localhost여야만 사용 가능합니다
        navigator.geolocation.getCurrentPosition(position => {
            console.log(position)
          }, err => console.log(err)
          );
    })

  const test00013 = () => {
    let randomValue = randomString();

    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/get_profile`,{
      verify_code_number: 'skadlfslaglasotpdy!!!'
    },{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      // console.log(res)
      const {data} = res
      console.log(data)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00001 = () => {
    let randomValue = randomString();
    let params = {
      address: '서울시 강서구',
      road_address: '도로명',
      latitude: 37.5182112402056,
      longtitude: 127.023150432187
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/update_address`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00002 = () => {
    let randomValue = randomString();
    let params = {
        latitude: 37.50696591466534,
        longtitude: 127.05267463391276
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/get_parking_lot`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }


  const test00031 = () => {
    let randomValue = randomString();
    let params = {
        parking_lot_index: 1,
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/detail_parking_lot`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00003 = () => {
    let randomValue = randomString();
    let params = {
        name: '노정우',
        phone: '01000000000',
        address: '서울시 강서구...',
        car_type: 0,
        car_value: 'sdf',
        period: 0, //0: 3개월 미만, 1: 3개월 이상, 2: 6개월 이상, 3: 12개월 이상
        start_date: '5000-09-20',
        price: 6000000,
        option: '지붕이 있는곳으로 부탁드려요!'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/request_finding`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }
  
  const test00004 = () => {
    let randomValue = randomString();
    let params = {
        parking_lot_index : 74,
        car_type: 0,
        car_value: 'bmw',
        car_code: 0,
        car_name: '123',
        car_number: '123',
        name: 'test',
        phone_number: '01021835762'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/request_reserving`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

    
  const test00005 = () => {
    let randomValue = randomString();
    let params = {}
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/get_usage_list`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00006 = () => {
    let randomValue = randomString();
    let params = {
        request_reserving_index: 137,
        parking_lot_index: 4,
        car_type: 0,
        car_value: 'sdf',
        merchant_uid: 'juchating_reserving_7_1626677738350_1',
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/cancel_reserve`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }


  const test00007 = () => {
    let randomValue = randomString();
    let params = {
        phone: '01021835762',
        type:2
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/send_phone_number`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00017 = () => {
    let randomValue = randomString();
    let params = {
        phone: '01021835762',
        verify_number: 5939
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/verify_phone_number`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00016 = () => {
    let randomValue = randomString();
    let params = {}
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/change_phone_number`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00011 = () => {
    let randomValue = randomString();
    let params = {}
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/etc/get_notice`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const uploadImage = () => {
    const formData = new FormData();
    formData.append("img", files[0]);
    console.log(files)
    axios.post(`${process.env.REACT_APP_API_HOST}/upload`, formData,{
        headers : {
            'Content-Type': 'multipart/form-data'
        }
      })
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }

  
  const test00019 = () => {
    let randomValue = randomString();
    let params = {
        imp_uid: 'imps_808233166340',
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/import/check_payment`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00020 = () => {
    let randomValue = randomString();
    let params = {
        imp_uid: 'imps_808233166340',
        billing_res : '{"success":true,"imp_uid":"imp_666969834449","pay_method":"card","merchant_uid":"juchating_reserving_7_1624513925415bd907","name":"test1","paid_amount":100,"currency":"KRW","pg_provider":"html5_inicis","pg_type":"payment","pg_tid":"StdpayBillINIBillTst20210624145406286598","apply_num":"","buyer_name":"test","buyer_email":"iamport@siot.do","buyer_tel":"02-1234-1234","buyer_addr":"","buyer_postcode":"","custom_data":null,"status":"paid","paid_at":1624514046,"receipt_url":"https://iniweb.inicis.com/DefaultWebApp/mall/cr/cm/mCmReceipt_head.jsp?noTid=StdpayBillINIBillTst20210624145406286598&noMethod=1","card_name":"삼성카드","bank_name":null,"card_quota":0,"card_number":"379183*********","customer_uid":"njw920@naver.com_1624513969128"}'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/import/update_key_pay`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  
  const test00021 = () => {
    let randomValue = randomString();
    let params = {
        merchant_uid: 'juchating_reserving_7_1625549938858_1',
        card_name: '삼성카드',
        card_number: '379183*********',
        amount: 100,
        name: '정기 api 결제 테스트'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/import/api_pay`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00033 = () => {
    let randomValue = randomString();
    let params = {
        merchant_uid: 'juchating_reserving_7_1626831118959_1',
        card_name: '삼성카드',
        card_number: '379183*********',
        amount: 100,
        name: '정기 api 결제 테스트',
        status: 9,
        request_reserving_index: 144

    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/import/api_pay_extension`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00032 = () => {
    let randomValue = randomString();
    let params = {
        merchant_uid: 'juchating_reserving_7_1626831118959_1',
        card_name: '삼성카드',
        card_number: '379183*********',
        amount: 100,
        name: '정기 api 결제 테스트',
        status: 9,
        request_reserving_index: 144

    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/import/api_pay_extension`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00022 = () => {
    let randomValue = randomString();
    let params = {
        request_finding_index: 40,
        merchant_uid: 'juchating_finding_7_1625124279438'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/cancel_find`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00018 = () => {
    let randomValue = randomString();
    let params = {
        request_finding_index: 34,
        merchant_uid: 'juchating_finding_7_1624516768292eb10f'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/cancel_find_pay`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  // postcode->kakao map을 이용해 좌표를 가져오거나, 현위치 가져오기로 좌표를 가져올때 실행하는 api 입니다.
  // 좌표값이 없을경우 실행하지 않습니다
  const test00024 = () => {
    let randomValue = randomString();
    let params = {
      latitude: 37.5182112402056,
      longtitude: 127.023150432187
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/etc/log_address`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }


  const test00015 = () => {
    let randomValue = randomString();
    let params = {
      request_reserving_index: 144,
      parking_lot_index: 74,
      car_type: 1,
      car_value: 22
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/stop_reserve`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00008 = () => {
    let randomValue = randomString();

  
    const formData = new FormData();
    formData.append("car_img", files[0]);
    formData.append("name", '노정우');
    formData.append("car_name", 'bmw');
    formData.append("car_number", '차 번호');
    formData.append("request_reserving_index", '144');
    formData.append("merchant_uid", 'juchating_reserving_7_1626831118959_1');

    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/register_car`, formData,{
      withCredentials : true,  
      headers : {
            'Content-Type': 'multipart/form-data',
            randomValue: randomValue
        }
      })
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }

  
  const test00009 = () => {
    let randomValue = randomString();

    const formData = new FormData();
    formData.append("car_img", files[0]);
    formData.append("name", '노정우');
    formData.append("car_name", '차명 수정1111111');
    formData.append("car_number", '차 번호');
    formData.append("request_reserving_index", '114');
    formData.append("merchant_uid", 'juchating_reserving_7_1625810907353_2');
    formData.append("request_car_index", '14');

    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/revise_car`, formData,{
      withCredentials : true,  
      headers : {
            'Content-Type': 'multipart/form-data',
            randomValue: randomValue
        }
      })
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }

  const test00025 = () => {
    let randomValue = randomString();

    const formData = new FormData();
    formData.append("car_img", files[0]);
    formData.append("name", '노정우');
    formData.append("car_name", 'asdasdasdåå');
    formData.append("car_number", '노12323정우');

    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/request_car_register`, formData,{
      withCredentials : true,  
      headers : {
            'Content-Type': 'multipart/form-data',
            randomValue: randomValue
        }
      })
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }

  const test00026 = () => {
    let randomValue = randomString();

    const formData = new FormData();
    formData.append("car_img", files[0]);
    formData.append("name", '노정우 변경완료ㄴㄴㄴㄴㄴㄴ');
    formData.append("car_name", '123');
    formData.append("car_number", '노12323정우');
    formData.append("request_car_index", '15');

    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/revise_car_register_req`, formData,{
      withCredentials : true,  
      headers : {
            'Content-Type': 'multipart/form-data',
            randomValue: randomValue
        }
      })
      .then(res => {
        console.log(res)
      })
      .catch(res => {
        console.log(res)
      })
  }

  const test00027 = () => {
    let randomValue = randomString();
    let params = {
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/user/car_list`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00028 = () => {
    let randomValue = randomString();
    let params = {
      request_car_index: 1,
      car_name: '123',
      car_number: '123',
      car_code: 1,
      request_reserving_index: 95,
      merchant_uid:'juchating_reserving_7_1625555851882_1',
      parking_lot_index: 4
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/select_car`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00029 = () => {
    let randomValue = randomString();
    let params = {
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/etc/card_list`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }

  const test00030 = () => {
    let randomValue = randomString();
    let params = {
      card_name: '삼성카드',
      card_number: '379183*********'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/etc/set_represent_card`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      console.log(res)
    })
    .catch(res => {
      console.log(res)
    })
  }


  const test00034 = () => {
    let randomValue = randomString();
    let params = {
      request_reserving_index : 144,
      merchant_uid: 'juchating_reserving_7_1626831118959_1'
    }
    document.cookie = `randomValue=${randomValue}`
    axios.post(`${process.env.REACT_APP_API_HOST}/parking_lot/get_using_park_img`,params,{
      withCredentials : true,
      headers : {
        randomValue: randomValue}
    })
    .then(res => {
      const {data} = res, img_ob = data[1]
      let img_arr: Array<string> = []
      Object.keys(img_ob).map(v => {
        const blob = new Blob([Buffer.from(img_ob[v].Body)], {
          type: img_ob[v].ContentType
      });
        img_arr.push(URL.createObjectURL(blob))
      })
      setImg(img_arr)
    })
    .catch(res => {
      console.log(res)
    })
  }

    //발품 신청시 결제는 일반 결제이기에 응답을 받은후 #00019api 호출을 하시면 됩니다
    // 샘플 코드는 아래와 같습니다
    const findingPay = () => {
        const {IMP}: any = window;
        IMP.request_pay({
            pg : 'inicis', // version 1.1.0부터 지원.
            pay_method : 'card',
            merchant_uid : `merchant_uid`, //내려지 merchant_uid 값,
            name : '주문명:결제테스트',
            amount : 100,
            buyer_email : 'iamport@siot.do',
            buyer_name : '구매자이름',
            buyer_tel : '010-1234-5678',
            buyer_addr : '서울특별시 강남구 삼성동',
            buyer_postcode : '123-456',
            m_redirect_url : process.env.REACT_APP_IMPORT_REDIRECT_URL
        }, function(rsp:any) {
            if ( rsp.success ) {
                var msg = '결제가 완료되었습니다.';
                msg += '고유ID : ' + rsp.imp_uid;
                msg += '상점 거래ID : ' + rsp.merchant_uid;
                msg += '결제 금액 : ' + rsp.paid_amount;
                msg += '카드 승인번호 : ' + rsp.apply_num;
            } else {
                var msg = '결제에 실패하였습니다.';
                msg += '에러내용 : ' + rsp.error_msg;
            }
            alert(msg);
        });
    }

//정기 결제 신청시 결과를 받으면 빌링키 등록과 결제 확인 api가 진행되야합니다
  const reservingPay = () => {
    const {IMP}: any = window;

    let email = 'njw920@naver.com' //사용자 이메일
    let date = new Date().getTime()
    IMP.request_pay({
        pg : "html5_inicis.INIBillTst", // 실제 계약 후에는 실제 상점아이디로 변경
        pay_method : 'card', // 'card'만 지원됩니다.
        merchant_uid : `merchant_uid`, //내려지 merchant_uid 값,
        name : 'my_name',
        amount : 100, // 결제창에 표시될 금액. 실제 승인이 이뤄지지는 않습니다. (모바일에서는 가격이 표시되지 않음)
        customer_uid : `${email}_${date}`, //customer_uid 파라메터가 있어야 빌링키 발급을 시도합니다.
        buyer_email : 'iamport@siot.do',
        buyer_name : 'test',
        buyer_tel : '02-1234-1234'
    }, function(rsp: any) {
        if ( rsp.success ) {
            console.log(rsp)
            alert('빌링키 발급 성공');
        } else {
            alert('빌링키 발급 실패');
        }
    });

  }

    return (
        <>  
            <Header/>
            <h1>ApiTest page</h1>
                <br/>
                <br/>
            
        
            <button onClick={test00013}>#00013</button>
            <br/>
            <button onClick={test00031}>#00031</button>
            <br/>
            <br/>
            <button onClick={test00001}>#00001</button>
            <br/>
            <button onClick={test00002}>#00002</button>
            <br/>
            <button onClick={test00003}>#00003</button>
            <br/>
            <button onClick={test00004}>#00004</button>
            <br/>
            <button onClick={test00005}>#00005</button>
            <br/>
            <button onClick={test00006}>#00006</button>
            <br/>
            <button onClick={test00007}>#00007</button>
            <br/>
            <button onClick={test00017}>#00017</button>
            <br/>
            <button onClick={test00016}>#00016</button>
            <br/>
            <button onClick={test00011}>#00011</button>
            <br/>
            <button onClick={test00019}>#00019</button>
            <br/>
            <button onClick={test00020}>#00020</button>
            <br/>
            <button onClick={test00033}>#00033</button>
            <br/>
            <button onClick={test00032}>#00032</button>
            <br/>
            <br/>
            <button onClick={test00021}>#00021</button>
            <br/>
            <button onClick={test00022}>#00022</button>
            <br/>
            <button onClick={test00018}>#00018</button>
            <br/>
            <button onClick={test00015}>#00015</button>
            <br/>
            <br/>
            <button onClick={test00024}>#00024</button>
            <br/>
            <button onClick={test00008}>#00008</button>
            <br/>
            <button onClick={test00009}>#00009</button>
            <br/>
            <button onClick={test00025}>#00025</button>
            <br/>
            <button onClick={test00026}>#00026</button>
            <br/>
            <button onClick={test00027}>#00027</button>
            <br/>
            <br/>
            <button onClick={test00028}>#00028</button>
            <br/>
                <input type="file" onChange={(e:any) => setFiles(e.target.files)} multiple/>
                <button onClick={uploadImage}>uploadImage</button>
            <br/>
            <br/>
            <br/>
            <button onClick={test00029}>#00029</button>
            <br/>
            <button onClick={test00030}>#00030</button>
            <br/>
            <button onClick={test00034}>#00034</button>
            {img ? img.map((v, i) => <img key ={i} src={v} loading="lazy" onLoad={() => URL.revokeObjectURL(v)}/>) : null}

        </>
    );
}