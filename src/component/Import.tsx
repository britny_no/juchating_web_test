import react, {useState, useEffect} from 'react'
import { Link } from "react-router-dom"
import KakaoLogin from 'react-kakao-login';
import axios from 'axios'
import { GoogleLogin } from 'react-google-login';
import cryptoRandomString from 'crypto-random-string'
import Header from "./Header"



export default function KLogin() {
    const[billingState, setBillingState] = useState('')

    const {IMP}: any = window;
    
    const buy = () => {
        IMP.request_pay({
            pg : 'inicis', // version 1.1.0부터 지원.
            pay_method : 'card',
            merchant_uid : `juchating_finding_7_1624943546617s58545`, //내려지 merchant_uid 값,
            name : '주문명:결제테스트1',
            amount : 100,
            buyer_email : 'iamport@siot.do',
            buyer_name : '구매자이름',
            buyer_tel : '010-1234-5678',
            buyer_addr : '서울특별시 강남구 삼성동',
            buyer_postcode : '123-456',
            m_redirect_url : process.env.REACT_APP_IMPORT_REDIRECT_URL
        }, function(rsp:any) {
            if ( rsp.success ) {
                var msg = '결제가 완료되었습니다.';
                msg += '고유ID : ' + rsp.imp_uid;
                msg += '상점 거래ID : ' + rsp.merchant_uid;
                msg += '결제 금액 : ' + rsp.paid_amount;
                msg += '카드 승인번호 : ' + rsp.apply_num;
            } else {
                var msg = '결제에 실패하였습니다.';
                msg += '에러내용 : ' + rsp.error_msg;
            }
            alert(msg);
        });
    }

    const getBilling = () => {
        let email = 'njw920@naver.com' //사용자 이메일
        let date = new Date().getTime()
        IMP.request_pay({
            pg : "html5_inicis.INIBillTst", // 실제 계약 후에는 실제 상점아이디로 변경
            pay_method : 'card', // 'card'만 지원됩니다.
            merchant_uid : `juchating_reserving_7_1624513925415bd907s`, //내려지 merchant_uid 값,
            name : 'test1',
            amount : 0, // 결제창에 표시될 금액. 실제 승인이 이뤄지지는 않습니다. (모바일에서는 가격이 표시되지 않음)
            customer_uid : `${email}_${date}`, //customer_uid 파라메터가 있어야 빌링키 발급을 시도합니다.
            buyer_email : 'iamport@siot.do',
            buyer_name : 'test',
            buyer_tel : '02-1234-1234'
        }, function(rsp: any) {
            if ( rsp.success ) {
                console.log(rsp)
                alert('빌링키 발급 성공');
                setBillingState(JSON.stringify(rsp))
            } else {
                alert('빌링키 발급 실패');
            }
        });
    }
    return (
        <>  
            <Header/>
            <h1>IM port page</h1>
            <button onClick={buy}>결제하기</button>
            <br/>
            <br/>
            {billingState}
            <br/>
            <button onClick={getBilling}>빌링키 발급</button>
            <br/>
     
        </>
    );
}