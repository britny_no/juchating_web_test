import React, {useEffect} from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import axios from 'axios';
import cryptoRandomString from 'crypto-random-string';

import Home from './component/Home'
import Klogin from './component/KakaoLogin'
import GLogin from './component/GoogleLogin'
import NLogin from './component/NaverLogin'
import PostCode from './component/PostCode'
import Import from './component/Import'
import PhoneVerify from './component/PhoneVerify'
import Security from './component/Security'
import ApiTest from './component/ApiTest'
import Pdf from './component/Pdf'


function App() {
  useEffect(() => {
  }, [])
  return (
    <BrowserRouter>
          <Route exact={true} path="/" component={Home} />
          <Route exact={true} path="/klogin" component={Klogin} />
          <Route exact={true} path="/glogin" component={GLogin} />
          <Route exact={true} path="/nlogin" component={NLogin} />
          <Route exact={true} path="/postcode" component={PostCode} />
          <Route exact={true} path="/import" component={Import} />
          <Route exact={true} path="/phone_verify" component={PhoneVerify} />
          <Route exact={true} path="/security" component={Security} />
          <Route exact={true} path="/api_test" component={ApiTest} />
          <Route exact={true} path="/pdf" component={Pdf} />
          
    </BrowserRouter>
  );
}

export default App;
