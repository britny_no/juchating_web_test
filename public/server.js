const express = require("express");
const path = require("path");
const app = express();
const axios = require('axios')
const request = require('request');
const bodyParser = require('body-parser')
const dotenv = require('dotenv');

// config
dotenv.config({ path: path.normalize(__dirname+'/../.env') });
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
app.use(express.static(__dirname));


// routes
const kakao = require('./routes/kakao')
const naver = require('./routes/naver')


app.use('/kakao', kakao);
app.use('/naver', naver);



//@@@@@@@@@@@@@@@ import
app.get('/import_redirect', (req, res) => {
  console.log(res.query)
  res.send('import')
})

app.post('/import_redirect', (req, res) => {
  console.log(res.body)
  res.send('import post')
})


// @@@@@@@@@@@@@@@ kakao




// @@@@@@@@@@@@@@@ naver

//  app.post('/naver_member', function (req, res) {
//   const header = "Bearer " + req.body.token;
//   const api_url = 'https://openapi.naver.com/v1/nid/me';
//   const options = {
//       url: api_url,
//       headers: {'Authorization': header}
//    };
//   request.get(options, function (error, response, body) {
//     if (!error && response.statusCode == 200) {
//       res.writeHead(200, {'Content-Type': 'text/json;charset=utf-8'});
//       res.end(body);
//     } else {
//       console.log('error');
//       if(response != null) {
//         res.status(response.statusCode).end();
//         console.log('error = ' + error);
//         console.log( response.body);
//       }
//     }
//   });
// });

// app.get("/naver_redirect", async function (req, res) {
//   const client_id = process.env.REACT_APP_NAVER_KEY;
//   const client_secret = process.env.REACT_APP_NAVER_SECRET_KEY;
//   const redirectURI = encodeURI(process.env.REACT_APP_NAVER_CALLBACK_URL);
//   const code = req.query.code;
//   const state = req.query.state;

//   api_url = 'https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id='
//      + client_id + '&client_secret=' + client_secret + '&redirect_uri=' + redirectURI + '&code=' + code + '&state=' + state;
//     const options = {
//         url: api_url,
//         headers: {'X-Naver-Client-Id':client_id, 'X-Naver-Client-Secret': client_secret}
//      };
//     request.get(options, function (error, response, body) {
//       if (!error && response.statusCode == 200) {
//         let data = JSON.parse(body)
//         res.render('NaverLogin', {access_token: data.access_token, refresh_token: data.refresh_token})
//       } else {
//         res.status(response.statusCode).end();
//         console.log('error = ' + response.statusCode);
//       }
//   });
// })

// app.get('/naver_refresh_access', (req, res) => {
//   const client_id = process.env.REACT_APP_NAVER_KEY;
//   const client_secret = process.env.REACT_APP_NAVER_SECRET_KEY;
//   const options = {
//     url: `https://nid.naver.com/oauth2.0/token?grant_type=refresh_token&client_id=${client_id}&client_secret=${client_secret}&refresh_token=${req.query.refresh_token}`,
//  };
//   request.get(options, function (error, response, body) {
//     if (!error && response.statusCode == 200) {
//       res.send(body)
//     } else {
//       res.status(response.statusCode).end();
//       console.log('error = ' + response.statusCode);
//     }
//   })


// })



app.use('*', function (req, res, next) {
  console.log('Request Type:', req.originalUrl);
  if(req.baseUrl !== req.originalUrl){
    res.status(404)
  } else{
    next();
  }
});

app.get("*", function(req, res) {
  res.sendFile(path.resolve(__dirname, "index.html"));
});
function handleListenLog() {
  console.log(`Server Starting...${process.env.PORT}`);
}
app.listen(process.env.PORT, handleListenLog);