const express = require("express");
const mysql = require("mysql");
const axios = require("axios");
const moment = require('moment'); 
const path = require('path'); 
const jwt = require('jsonwebtoken')
const kakaoRoute = express.Router();


//config
require('moment-timezone'); 
moment.tz.setDefault("Asia/Seoul"); 

//lib
const dbConfig = require('../lib/db')

//js
const kakao = require('../server_js/kakao')
const common = require('../server_js/common')


kakaoRoute.get("/login_redirect", async function (req, res) {
    const code = req.query.code
    axios({   
      method: 'POST',
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
      },
      url: 'https://kauth.kakao.com/oauth/token',
      data: kakao.makeFormData({
        grant_type: 'authorization_code',
        client_id: process.env.REACT_APP_KAKAO_JS_KEY,
        redirect_uri: process.env.REACT_APP_KAKAO_REDIRECT_URL,
        code,
      })
    })
    .then((response) => {
      const oauth_data = response.data

      //get profile
      axios({   
        method: 'POST',
        headers: {
          'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
          'Authorization': `Bearer ${oauth_data.access_token}`
        },
        url: 'https://kapi.kakao.com/v2/user/me',
      })
      .then((response) => {
        let profile_data = response.data, email = profile_data.kakao_account.email

        // db update or insert
        let connection = mysql.createConnection(dbConfig), params = [email, moment().format('YYYY-MM-DD HH:mm:ss'), oauth_data.refresh_token, oauth_data.access_token, oauth_data.refresh_token, oauth_data.access_token]
        connection.connect();
        connection.query(`INSERT INTO member (email, register_date, refresh_token_kakao, access_token_kakao ) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE  refresh_token_kakao= ?, access_token_kakao = ?`, params,  async function (error, results, fields) {
            connection.end();
            if (error) {
                common.writeLog(path.normalize(__dirname+'/../../db_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
                res.render('Error')
            } else {
              const jwt_result = jwt.sign({
                type: 'kakao',
                access_token: oauth_data.access_token,
                email: email,
                member_index:results.insertId
              }, process.env.JWT_SECRET, { expiresIn: '20m' });
              res.cookie(await common.getCacheKey('login'), jwt_result, { httpOnly: true, maxAge: 3700000 });
              res.render('KaKaoLogin')
            }
        });
      })
      .catch((error) => {
        common.writeLog(path.normalize(__dirname+'/../../kakao_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
        res.render('Error')
      })
    })
    .catch((error) => {
      common.writeLog(path.normalize(__dirname+'/../../kakao_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
      res.render('Error')
    })
  })
  

module.exports = kakaoRoute


