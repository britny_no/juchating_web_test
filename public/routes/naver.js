const express = require("express");
const mysql = require("mysql");
const request = require("request");
const moment = require('moment'); 
const path = require('path'); 
const jwt = require('jsonwebtoken')
const naverRouter = express.Router();


//config
require('moment-timezone'); 
moment.tz.setDefault("Asia/Seoul"); 

//lib
const dbConfig = require('../lib/db')

//js
const naver = require('../server_js/naver')
const common = require('../server_js/common')


naverRouter.get("/login_redirect", async function (req, res) {
    const client_id = process.env.REACT_APP_NAVER_KEY;
    const client_secret = process.env.REACT_APP_NAVER_SECRET_KEY;
    const redirectURI = encodeURI(process.env.REACT_APP_NAVER_CALLBACK_URL);
    const code = req.query.code;
    const state = req.query.state;

    api_url = 'https://nid.naver.com/oauth2.0/token?grant_type=authorization_code&client_id='
        + client_id + '&client_secret=' + client_secret + '&redirect_uri=' + redirectURI + '&code=' + code + '&state=' + state;
        const options = {
            url: api_url,
            headers: {'X-Naver-Client-Id':client_id, 'X-Naver-Client-Secret': client_secret}
        };
        request.get(options, function (error, response, body) {
        if (!error && response.statusCode == 200) {
            const oauth_data = JSON.parse(body)
            const header = "Bearer " + oauth_data.access_token;
            const api_url = 'https://openapi.naver.com/v1/nid/me';
            const options = {
                url: api_url,
                headers: {'Authorization': header}
            };
            request.get(options, function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    const member_data = JSON.parse(body), email = member_data.response.email

                    // db update or insert
                    let connection = mysql.createConnection(dbConfig), params = [email, moment().format('YYYY-MM-DD HH:mm:ss'), oauth_data.refresh_token, oauth_data.access_token, oauth_data.refresh_token, oauth_data.access_token]
                    connection.connect();
                    connection.query(`INSERT INTO member (email, register_date, refresh_token_naver, access_token_naver ) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE  refresh_token_naver= ?, access_token_naver = ?`, params,  async function (error, results, fields) {
                        connection.end();
                        if (error) {
                            common.writeLog(path.normalize(__dirname+'/../db_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
                            res.render('Error')
                        } else {
                        
                        const jwt_result = jwt.sign({
                            type: 'naver',
                            access_token: oauth_data.access_token,
                            email: email,
                            member_index:results.insertId
                        }, process.env.JWT_SECRET, { expiresIn: '20m' });
                        res.cookie(await common.getCacheKey('login'), jwt_result, { httpOnly: true, maxAge: 3700000 });
                        res.render('NaverLogin')
                        }
                    });
                } else {
                if(response != null) {
                    common.writeLog(path.normalize(__dirname+'/../../kakao_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
                    res.render('Error')
                }
                }
            });
        } else {
            common.writeLog(path.normalize(__dirname+'/../../kakao_err.txt'), `url:/kakao/login_redirect, ${JSON.stringify(error)}`)
            res.render('Error')
        }
    });
  })
  

module.exports = naverRouter


