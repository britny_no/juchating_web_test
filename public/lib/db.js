const dotenv = require('dotenv');
const path = require("path");
// config
dotenv.config({ path: path.normalize(__dirname+'/../../.env') });

const dbconfig = {
    host     : process.env.DB_HOST,
    user     : process.env.DB_ID,
    password : process.env.DB_PW,
    database : process.env.DB_DB,
}


module.exports = dbconfig
