const mysql = require('mysql')
const path = require('path')


const dbConfig = require('../lib/db')
const common = require('./common')

const getKey = (name) => {
    return new Promise((resolve, reject) => {
        const connection = mysql.createConnection(dbConfig), params= [name]
        const url = `select value  from key_value where name = ?`

        if(!params.includes(undefined)){
            connection.connect();
            connection.query(url, params,  function (error, results, fields) {
                connection.end();
                if (error) {
                    common.writeLog(path.normalize(__dirname+'/../../db_err.txt'), `postiion :/js/mysql/getKey, ${JSON.stringify(error)}`)
                    reject('잘못된 요청입니다');
                } else {
                    resolve(results[0].value)
                }
            });
        }else{
            reject('빈값이 있습니다')
        }
    })
}

module.exports = {
    getKey
}