const fs = require('fs');
const path = require("path");
const dotenv = require('dotenv');
const jwt = require('jsonwebtoken')
const cacheManager = require('cache-manager');


const mysql = require('./mysql')
// config
const memoryCache = cacheManager.caching({store: 'memory', ttl: 18000/*seconds*/})
dotenv.config({ path: path.normalize(__dirname+'/../../.env') });


const writeLog = (path, data, callback = () => {}) => {
    fs.open(path, "a", (err, fd) => {
        if (err) {
            console.log(err)
        }
        fs.appendFile(path, '\n' + data, function (err) {
            if (err) throw err;
            callback()
            return
        });
    })
}

const checkJwt = (token) => {
    return jwt.verify(token, process.env.JWT_SECRET, function(err, decoded) {
        if(err){
            return 0
        } else {
            return 1
        }
    });
}


const getCacheKey = (name) => {
    return new Promise(async (resolve, reject) => {
        try{
            const login_key = await memoryCache.get('login_key')
            if(login_key){
                resolve(login_key)
            } else {
                let key = await mysql.getKey(name)
                await memoryCache.set('login_key', key)
                resolve(key)
            }   
        }catch(err){
            reject
        }
    })
}



module.exports = {
    writeLog,
    checkJwt,
    getCacheKey

}
