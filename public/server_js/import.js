const axios = require('axios')

const paymentResult = async (memoryCache, imp_number) => {
  const access_token = await memoryCache.get('import_access_token')

  if(access_token){
     return checkPaymentResult(imp_number, access_token )
     .then( async res => {
        switch (res.code) {
          case 0:
            return [1, res.response];
          case -1:
            const access_res = await getAccessToken()
            if(access_res.code === 0){
              const access_token = access_res.response.access_token
              memoryCache.set('import_access_token', access_token)
              return checkPaymentResult(imp_number, access_token )
                    .then(res => res.code === 0 ? [1, res.response] : [-1, res.message])
                    .catch(err => [-1, JSON.stringify(err)])
            } else {
              return [-1, access_res.message];
            }
          default:
            return [-1, res.message];
        }
     })
     .catch(err => [-1, 'Error happening'])
 
  } else {
    const access_res = await getAccessToken()
    if(access_res.code === 0){
      const access_token = access_res.response.access_token
      memoryCache.set('import_access_token', access_token)
      return checkPaymentResult(imp_number, access_token )
            .then(res => res.code === 0 ? [1, res.response] : [-1, res.message])
            .catch(err => [-1, JSON.stringify(err)])
    } else {
      return [-1, access_res.message];
    }
  }
}


// get access token in import
const getAccessToken =  () => axios({
  url: "https://api.iamport.kr/users/getToken",
  method: "post", 
  headers: { "Content-Type": "application/json" }, 
  data: {
    imp_key: process.env.REACT_APP_IMPORT_API_KEY,
    imp_secret: process.env.REACT_APP_IMPORT_SECRET_KEY 
  }
})
.then(res => res.data)
.catch(err => err.response.data)

// check payment result in import
const checkPaymentResult = (imp_uid, access_token) => axios({
  url:`https://api.iamport.kr/payments/${imp_uid}`,
  method: "get", 
  headers: { "Authorization": access_token } 
})
.then(res => res.data)
.catch(err =>err.response.data)



module.exports = {    
  paymentResult,
  getAccessToken
}