

// for kakao login
const makeFormData = params => {
  const searchParams = new URLSearchParams()
  Object.keys(params).forEach(key => {
    searchParams.append(key, params[key])
  })

  return searchParams
}



module.exports = {
  makeFormData,
}